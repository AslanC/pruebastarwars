const axios = require('axios');

async function post(url, data, headers) {
  return await axios.default.post(url, data, { headers });
}

async function get(url, service ,headers, qs) {
  return await axios.default.get(url + service, { headers, params: qs });
}

module.exports = {
  post,
  get,
};