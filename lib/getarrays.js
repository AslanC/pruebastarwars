const {objPlanet} = require('../class/objplanets');
const {objCharacters} = require('../class/objcharacters');
const {objStarship} = require('../class/objstarships');
const {largestStarship} = require('../lib/largeststarship');

async function getArrays(planets, prototype){
    try {
        const array = await objsArray(planets,prototype);
        return array;    
    } catch (error) {
        throw error;
    }
    
}

async function objsArray(arrays, prototype){
    let array = [];
    try {
        switch (prototype) {
            case 0:
                await Promise.all(arrays.map(async(uri) => {
                    array.push(await objPlanet(uri));
                }));
                break;
            case 1:
                await Promise.all(arrays.map(async(uri) => {
                    array.push(await objCharacters(uri));
                }));
                break;
            case 2:
                await Promise.all(arrays.map(async(uri) => {
                    array.push(await objStarship(uri));
                }));
                array = await largestStarship(array);
            default:
                break;
        }
        return array;    
    } catch (error) {
        throw error;
    }

}

module.exports = {
    getArrays
}