
async function largestStarship(starships){
    let largest = 0;
    let r;
    return await new Promise((resolve, reject) => {
        starships.forEach((element, index) => {
            if(parseFloat(element.lengt) > parseFloat(largest)){
                largest = element.lengt;
                r = element;
            }
            if(starships.length === index + 1){
                var obj = {
                    name: r.name,
                    model: r.model,
                    manufacturer: r.manufacturer,
                    passengers: r.passengers,
                }
                resolve(r);
            }
        });
    });
}

module.exports = {
    largestStarship
}