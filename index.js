const http = require('http');
const config = require('./config/index');
const conection = require('./services/films');

const server = http.createServer((req, res) => {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');
    res.end('Hello Not World');
  });

  conection.getFilms();
  
  server.listen(config.port, config.host, () => {
    console.log(`Server on at http://${config.host}:${config.port}/`);
  });
