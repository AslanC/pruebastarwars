const {get} = require('../lib/request');

async function objCharacters(uri){
    try {        
        let res = await get(uri, '', {}, null);
        const homeWorld = await (await get(res.data.homeworld, '', {}, null)).data.name;
        const specie = await get(res.data.species, '', {}, null);
        var obj = {
        name: res.data.name,
        gender: res.data.gender,
        hair_color: res.data.hair_color,
        skin_color: res.data.skin_color,
        eye_color: res.data.eye_color,
        homeworld: homeWorld,
        species: {
            name: specie.data.name,
            language: specie.data.language,
            average_height: specie.data.average_height

         }
        }
        return obj
    } catch (error) {
        throw error;
    }
}

module.exports = {
    objCharacters
}