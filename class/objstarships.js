const {get} = require('../lib/request');

async function objStarship(item){
    try {
        let res = await get(item, '', {}, null);
        var obj = {
        name: res.data.name,
        model: res.data.model,
        manufacturer: res.data.manufacturer,
        passengers: res.data.passengers,
        lengt: res.data.length
        }
        return obj
    } catch (error) {
        throw error;
    }
}

module.exports = {
    objStarship
}